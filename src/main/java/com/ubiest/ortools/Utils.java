package com.ubiest.ortools;

import java.io.IOException;

import org.scijava.nativelib.NativeLoader;

public class Utils {

	public static void loadJniortools() {
		try {
			NativeLoader.loadLibrary("jniortools");
		} catch (IOException e) {
			System.out.println("" + e.getMessage());
		}
	}
}
